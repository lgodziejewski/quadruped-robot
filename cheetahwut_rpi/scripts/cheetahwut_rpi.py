#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64
from adafruit_rpi_code.Adafruit_PWM_Servo_Driver import PWM


def calculate_pwm_from_angle(angle):
    # TODO Formula to calculate PWM from angle
    pwm = angle
    return pwm


class I2CHandler:
    def __init__(self, bus, leg_number):
        self.bus = bus
        self.leg_number = leg_number
        rospy.Subscriber("cheetahwut/hip" + str(leg_number) + "_position_controller/command", Float64, self.callback, 0)
        rospy.Subscriber("cheetahwut/knee" + str(leg_number) + "_position_controller/command", Float64, self.callback, 4)

    def callback(self, msg, offset):
        rospy.loginfo(rospy.get_caller_id() + "I heard %lf", msg.data)
        # Calculate pwm width from sent angle data
        pwm = calculate_pwm_from_angle(msg.data)
        # setPWM(channel number, pwm start, pwm stop)
        # current numbers are leg 1: hip = 1, knee = 5, etc. for each leg (2,6; 3,7; 4,8)
        self.bus.setPWM(self.leg_number + offset, 0, pwm)


def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    bus = PWM(0x40, debug=True)
    leg1 = I2CHandler(bus, 1)
    leg2 = I2CHandler(bus, 2)
    leg3 = I2CHandler(bus, 3)
    leg4 = I2CHandler(bus, 4)
    rospy.init_node('i2c_handler', anonymous=True)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    listener()
