#include <ros/ros.h>

#include <jointstateclass.h>
#include <leg.h>

#include <boost/foreach.hpp>

#include <gazebo_msgs/LinkStates.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose.h>

void linkStateReaderCallback(const gazebo_msgs::LinkStatesConstPtr& msg)
{
  //cheetahwut::base_link is at place 3(2) in list
  geometry_msgs::Point temp = ((geometry_msgs::Pose) msg->pose.at(2)).position;
  //printf("%lf; %lf; %lf\n", temp.x, temp.y, temp.z);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "CheetahWUTNode");

  ros::NodeHandle n;

  /*
   * Leg numeration in robots model FL = 1, FR = 2, RL = 3, RR = 4.
   */
  //Create and initialize leg vector
  std::vector<Leg*> leg_vector;
  leg_vector.push_back(new Leg(LegTypes::FrontLeft, 50, n));
  leg_vector.push_back(new Leg(LegTypes::FrontRight, 250, n));
  leg_vector.push_back(new Leg(LegTypes::RearLeft, 150, n));
  leg_vector.push_back(new Leg(LegTypes::RearRight, 350, n));

  BOOST_FOREACH (Leg* leg, leg_vector)
  {
    ROS_INFO("My leg type is: %d", (int) leg->getLegType());
  }

  //Loop rate - no quicker than X Hz.
  ros::Rate loop_rate(50);

  JointStateClass joint_states;

  //Initialize leg state subscribers
  ros::Subscriber leg_state_sub = n.subscribe("/cheetahwut/joint_states", 1, &JointStateClass::jointControllerStateCallback, &joint_states);
  ros::Subscriber base_link_position_sub = n.subscribe("/gazebo/link_states", 1, linkStateReaderCallback);

  while(ros::ok())
  {
    //process callbacks from subscribers
    ros::spinOnce();

    //Get info from joint_states and put it into legs.
    BOOST_FOREACH (Leg* leg, leg_vector)
    {
      leg->setCurrentAnglesAndRefreshPosition(joint_states.getAnglesForLegNumber((int) leg->getLegType()));
    }

    BOOST_FOREACH (Leg* leg, leg_vector)
    {
      //"blind" algorithm and new ff algorithm (logic is inside)
      if (1)
      {
        leg->doStep();
      }
    }

    loop_rate.sleep();
  }

  return 0;
}
