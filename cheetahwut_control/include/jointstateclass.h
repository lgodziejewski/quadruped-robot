#ifndef JOINTSTATECLASS_H
#define JOINTSTATECLASS_H

#include <sensor_msgs/JointState.h>

#include <leg.h>

class JointStateClass
{
public:
  JointStateClass();
  ~JointStateClass();

  void jointControllerStateCallback(const sensor_msgs::JointStatePtr& msg);
  JointPosition getAnglesForLegNumber(int number);
private:
  JointPosition leg_angles_[4];


};

#endif // JOINTSTATECLASS_H
